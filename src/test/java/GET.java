import io.restassured.RestAssured;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class GET {

    @BeforeTest
    public void setup() {
        RestAssured.baseURI = "https://reqres.in/";
    }

    @Test
    public void getList() {
        given().
                when()
                .get("/api/users?page=2")
                .then()
                .statusCode(200)
                .body(containsString("michael.lawson@reqres.in")).log().all();
    }

    @Test
    public void getSingleUser() {
        given().
                when()
                .get("/api/users/2")
                .then()
                .statusCode(200)
                .body(containsString("Weaver"))
                .log().all();
    }

    @Test
    public void createUser() {
        Map<String, String> user = new HashMap<>();
        user.put("name", "morpheus");
        user.put("job", "leader");

        given().contentType("application/json").body(user).when()
                .post("/api/users")
                .then()
                .statusCode(201)
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("leader"))
                .body("id", notNullValue())
                .body("createdAt", notNullValue())
                .log().all();
    }

    @Test
    public void updateUser() {
        Map<String, String> user = new HashMap<>();
        user.put("name", "morpheus");
        user.put("job", "zion resident");

        given().contentType("application/json").body(user).when()
                .put("/api/users/2")
                .then()
                .statusCode(200)
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("zion resident"))
                .body("id", nullValue())
                .body("createdAt", nullValue())
                .body("updatedAt", notNullValue())
                .log().all();
    }

    @Test
    public void deleteUser() {
        given().when()
                .delete("/api/user/2")
                .then()
                .statusCode(204)
                .log().all();
    }
}


